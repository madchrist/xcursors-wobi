all: wobi

wobi: src/cells.png src/make.sh src/index.theme
	$(MAKE) clean
	cd src && ./make.sh
	mv src/preview.png .
	mkdir wobi
	mv src/cursors wobi/
	cp src/index.theme wobi/

clean:
	rm -fr wobi preview.png src/cursors src/preview.png

install_user: wobi
	rm -fr ~/.icons/wobi
	mkdir -p ~/.icons/	
	cp -r wobi ~/.icons/

install: wobi
	rm -fr /usr/share/icons/wobi
	cp -r wobi /usr/share/icons/
