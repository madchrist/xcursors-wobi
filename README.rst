====================================
WOBI - White Outline, Black Interior
====================================

A minimalist hi-contrast black and white xcursor theme.



Features
========

* All cursors are monochrome black and white images
* No shadows
* Two sizes: 32 and 64 (although 64 is just 32 scaled by 2x)



Preview
=======

.. image:: preview.png



Install
=======

| As a user:
| ``$ make install_user``
| or
| ``$ cp -r wobi ~/.icons/``


| As root:
| ``# make install``



Using the theme
===============

| Various components (GTK, QT, X11, Wayland, Gnome, KDE, etc) may need to be configured
| Here are a few good sources of info:

* `Arch Wiki <https://wiki.archlinux.org/index.php/Cursor_themes>`_
* `Gentoo Wiki <https://wiki.gentoo.org/wiki/Cursor_themes>`_



Building
========

| You can rebuild the theme from its source in src/ with:
| ``make clean && make``


You need imagemagick and xcursorgen
