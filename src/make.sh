#!/bin/sh

# This script generates xcursor files from a single image containing all the cursors

# $CELLS image:
# An image made of an array of "cells"
# Each cell is 34x34 and contains a 32x32 image surrounded by a 1px black border
# The top and left borders each contain a single white pixel representing the position (x, y) of the cursor's hotspot


# Sources of cursor names
#  - XDG:     https://www.freedesktop.org/wiki/Specifications/cursor-spec/
#  - XFont:   https://tronche.com/gui/x/xlib/appendix/b/
#  - CSS:     https://developer.mozilla.org/en-US/docs/Web/CSS/cursor
#             Many cursors in common with XDG, but a few extra ones
#  - UNKNOWN: https://gitlab.gnome.org/GNOME/gtk/-/blob/master/gdk/x11/gdkcursor-x11.c
#             https://developer.gnome.org/gdk4/stable/gdk4-Cursors.html#gdk-cursor-new-from-name
#             https://doc.qt.io/qt-5/qcursor.html
#             https://code.woboq.org/qt5/qtbase/src/plugins/platforms/xcb/qxcbcursor.cpp.html
#             https://fedoraproject.org/wiki/Artwork/EchoCursors/NamingSpec
#             A lot of names appear in GTK/QT but it is not clear if they originate from there, almost all of them are just renames of XDG or XFont cursors
#  - HASHES:  https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=372277
#             https://fedoraproject.org/wiki/Artwork/EchoCursors/NamingSpec
#             https://wiki.archlinux.org/index.php/Cursor_themes
#             I could not find the source cursors for the hashes

# TODO
# - cross/tcross ?
# - row/col-resize different from ns/ew-resize ?



# extract the given cell and generate its xcursorgen config
cell2pngconfig () # <cell_x> <cell_y> <delay>
{
    # find the cell's position in pixels
    X=$(($1 * 34))
    Y=$(($2 * 34))

    # find xhot and yhot by searching for the white pixel in the top and left border, respectively
    for XHOT in {0..31}; do if test $(magick "${CELLS}" -format "%[pixel: u.p{$((${X} +1 +${XHOT})),${Y}}]" info:) = "srgba(255,255,255,1)"; then break; fi; done
    for YHOT in {0..31}; do if test $(magick "${CELLS}" -format "%[pixel: u.p{${X},$((${Y} +1 +${YHOT}))}]" info:) = "srgba(255,255,255,1)"; then break; fi; done

    # size 32
    convert "${CELLS}" -strip -crop "32x32+$((${X} +1))+$((${Y} +1))" +repage "${TMPDIR}/32_${X}_${Y}.png"
    echo "32 ${XHOT} ${YHOT} ${TMPDIR}/32_${X}_${Y}.png ${3}" >> "${TMPDIR}/config"

    # size 64 (32 scaled 2x)
    convert "${TMPDIR}/32_${X}_${Y}.png" -scale 200% "${TMPDIR}/64_${X}_${Y}.png"
    echo "64 $((${XHOT} *2)) $((${YHOT} *2)) ${TMPDIR}/64_${X}_${Y}.png ${3}" >> "${TMPDIR}/config"
}


# generate an xcursor file from a cell
xc_cell () # <cursor_name> <cell_x> <cell_y>
{
    rm -f "${TMPDIR}/config"
    cell2pngconfig $2 $3 ""
    xcursorgen "${TMPDIR}/config" "cursors/${1}"
}


# generate an xcursor animation file from multiple cells
# assumes an 8-frame animation with 125ms delay, stored in a column of 8 cells, with the top one given as parameter
xc_cell_ani () # <cursor_name> <cell_x> <cell_y> 
{
    rm -f "${TMPDIR}/config"
    for i in {0..7}; do cell2pngconfig $2 $(($3 + $i)) 125; done
    xcursorgen "${TMPDIR}/config" "cursors/${1}"
}
    

# redirect a cursor name to another
xc_link () # <cursor_name> <target_cursor_name>
{
    ln -s "${2}" "cursors/${1}"
}


# fallback for cursors with no images
xc_missing () # <cursor_name>
{
    if test -z $DEBUG; then 
        xc_link "${1}" "default"
    else
        xc_cell "${1}" 12 7  # an image of the word "missing"
    fi
}



# paths
CELLS='cells.png'
PREVIEW='preview.png'
TMPDIR='/tmp/wobi'


# generate a preview by removing the cell borders from $CELLS
convert "${CELLS}" -colorspace Gray "${PREVIEW}"
for X in {0..15}; do convert "${PREVIEW}" -fill gray -draw "rectangle $(($X *34 -1)),0 $(($X *34)),$((15*34 -1))" "${PREVIEW}"; done  # paint vertical borders in gray
for Y in {0..15}; do convert "${PREVIEW}" -fill gray -draw "rectangle 0,$(($Y *34 -1)) $((15*34 -1)),$(($Y *34))" "${PREVIEW}"; done  # paint horizontal borders in gray
convert "${PREVIEW}" -transparent gray "${PREVIEW}"  # replace gray with transparent (-draw cannot erase pixels)
convert \( -size $(convert "${PREVIEW}" -format "%wx%h" info:) tile:pattern:checkerboard \) "${PREVIEW}" -compose over -composite -scale 200% "${PREVIEW}"  # add a checkerboard background


# setup directories
rm -fr "${TMPDIR}" cursors
mkdir "${TMPDIR}" cursors


# generate the cursors
#           [ -- CURSOR NAME -- ] [ -- CONTENT --- ] [ SOURCE ]
xc_cell     "alias"                4 4               # XDG CSS
xc_cell     "all-scroll"          11 1               # XDG CSS
xc_link     "arrow"               "right_ptr"        # XFONT
xc_link     "ask"                 "help"             # UNKNOWN
xc_link     "base_arrow_down"     "based_arrow_down" # UNKNOWN
xc_link     "base_arrow_up"       "based_arrow_up"   # UNKNOWN
xc_link     "based-down"          "based_arrow_down" # UNKNOWN
xc_link     "based-up"            "based_arrow_up"   # UNKNOWN
xc_cell     "based_arrow_down"     7 7               # XFONT
xc_cell     "based_arrow_up"       7 6               # XFONT
xc_link     "basic-arrow"         "up-arrow"         # UNKNOWN
xc_link     "basic_arrow"         "up-arrow"         # UNKNOWN
xc_link     "bd_double_arrow"     "nwse-resize"      # UNKNOWN
xc_missing  "boat"                                   # XFONT
xc_missing  "bogosity"                               # XFONT
xc_link     "bottom-tee"          "bottom_tee"       # UNKNOWN
xc_link     "bottom_left_corner"  "sw-resize"        # XFONT
xc_link     "bottom_right_corner" "se-resize"        # XFONT
xc_link     "bottom_side"         "s-resize"         # XFONT
xc_cell     "bottom_tee"           8 5               # XFONT
xc_missing  "box_spiral"                             # XFONT
xc_cell     "cell"                 1 2               # XDG CSS
xc_link     "center_ptr"          "up-arrow"         # XFONT
xc_cell     "circle"               1 4               # XFONT
xc_missing  "clock"                                  # XFONT
xc_link     "closedhand"          "grabbing"         # UNKNOWN
xc_missing  "coffee_mug"                             # XFONT
xc_link     "col-resize"          "ew-resize"        # XDG CSS
xc_cell     "color-picker"         0 7               # UNKNOWN
xc_cell     "context-menu"         0 5               # XDG CSS
xc_cell     "copy"                 4 3               # XDG CSS
xc_link     "cross"               "crosshair"        # XFONT
xc_link     "cross_reverse"       "crosshair"        # XFONT
xc_link     "crossed_circle"      "not-allowed"      # UNKNOWN
xc_cell     "crosshair"            1 0               # XDG XFONT CSS
xc_cell     "default"              0 0               # XDG CSS
xc_cell     "diamond_cross"        1 1               # XFONT
xc_cell     "dnd-ask "             3 2               # UNKNOWN
xc_cell     "dnd-copy"             3 3               # UNKNOWN
xc_cell     "dnd-link"             3 4               # UNKNOWN
xc_cell     "dnd-move"             3 5               # UNKNOWN
xc_cell     "dnd-none"             3 6               # UNKNOWN
xc_cell     "dot"                  1 5               # XFONT
xc_link     "dot-box"             "dot_box_mask"     # UNKNOWN
xc_link     "dot_box"             "dot_box_mask"     # UNKNOWN
xc_cell     "dot_box_mask"         1 7               # XFONT
xc_link     "double-arrow"        "ns-resize"        # UNKNOWN
xc_link     "double_arrow"        "ns-resize"        # XFONT
xc_link     "down"                "sb_down_arrow"    # UNKNOWN
xc_link     "draft_large"         "right_ptr"        # XFONT
xc_link     "draft_small"         "right_ptr"        # XFONT
xc_link     "draped-box"          "draped_box"       # UNKNOWN
xc_missing  "draped_box"                             # XFONT
xc_cell     "e-resize"             9 1               # XDG CSS
xc_cell     "ew-resize"            8 7               # XDG CSS
xc_cell     "exchange"             2 4               # XFONT
xc_link     "fd_double_arrow"     "nesw-resize"      # UNKNOWN
xc_cell     "fleur"                2 5               # XFONT
xc_link     "forbidden"           "not-allowed"      # UNKNOWN
xc_missing  "gobbler"                                # XFONT
xc_cell     "grab"                 3 0               # CSS
xc_cell     "grabbing"             3 1               # CSS
xc_missing  "gumby"                                  # XFONT
xc_link     "h_double_arrow"      "ew-resize"        # UNKNOWN
xc_link     "half-busy"           "progress"         # UNKNOWN
xc_link     "hand"                "pointer"          # UNKNOWN
xc_cell     "hand1"                0 4               # XFONT
xc_link     "hand2"               "pointer"          # XFONT
xc_missing  "heart"                                  # XFONT
xc_cell     "help"                 4 2               # XDG CSS
xc_link     "ibeam"               "text"             # UNKNOWN
xc_missing  "icon"                                   # XFONT
xc_missing  "iron_cross"                             # XFONT
xc_link     "left"                "sb_left_arrow"    # UNKNOWN
xc_link     "left-hand"           "hand1"            # UNKNOWN
xc_link     "left-tee"            "left_tee"         # UNKNOWN
xc_link     "left_ptr"            "default"          # XFONT
xc_link     "left_ptr_watch"      "progress"         # UNKNOWN
xc_link     "left_side"           "w-resize"         # XFONT
xc_cell     "left_tee"             7 4               # XFONT
xc_missing  "leftbutton"                             # XFONT
xc_link     "link"                "alias"            # UNKNOWN
xc_cell     "ll_angle"             7 5               # XFONT
xc_cell     "lr_angle"             9 5               # XFONT
xc_missing  "man"                                    # XFONT
xc_missing  "middlebutton"                           # XFONT
xc_missing  "mouse"                                  # XFONT
xc_link     "move"                "fleur"            # CSS
xc_cell     "n-resize"             8 0               # XDG CSS
xc_cell     "ne-resize"            9 0               # XDG CSS
xc_cell     "nesw-resize"          9 6               # XDG CSS
xc_cell     "no-drop"              4 6               # XDG CSS
xc_cell     "not-allowed"          2 6               # XDG CSS
xc_cell     "ns-resize"            8 6               # XDG CSS
xc_cell     "nw-resize"            7 0               # XDG CSS
xc_cell     "nwse-resize"          9 7               # XDG CSS
xc_link     "openhand"            "grab"             # UNKNOWN
xc_cell     "pencil"               0 6               # XFONT
xc_cell     "pirate"               2 7               # XFONT
xc_link     "plus"                "cell"             # XFONT
xc_cell     "pointer"              0 3               # XDG CSS
xc_link     "pointing_hand"       "pointer"          # UNKNOWN
xc_cell_ani "progress"             5 0               # XDG CSS
xc_link     "question_arrow"      "help"             # XFONT
xc_link     "right"               "sb_right_arrow"   # UNKNOWN
xc_link     "right-tee"           "right_tee"        # UNKNOWN
xc_cell     "right_ptr"            0 2               # XFONT
xc_link     "right_side"          "e-resize"         # XFONT
xc_cell     "right_tee"            9 4               # XFONT
xc_missing  "rightbutton"                            # XFONT
xc_link     "row-resize"          "ns-resize"        # XDG CSS
xc_missing  "rtl_logo"                               # XFONT
xc_cell     "s-resize"             8 2               # XDG CSS
xc_missing  "sailboat"                               # XFONT
xc_cell     "sb_down_arrow"       11 2               # XFONT
xc_link     "sb_h_double_arrow"   "ew-resize"        # XFONT
xc_cell     "sb_left_arrow"       10 1               # XFONT
xc_cell     "sb_right_arrow"      12 1               # XFONT
xc_cell     "sb_up_arrow"         11 0               # XFONT
xc_link     "sb_v_double_arrow"   "ns-resize"        # XFONT
xc_cell     "se-resize"            9 2               # XDG CSS
xc_missing  "shuttle"                                # XFONT
xc_link     "size_all"            "fleur"            # UNKNOWN 
xc_link     "size_bdiag"          "nesw-resize"      # UNKNOWN
xc_link     "size_fdiag"          "nwse-resize"      # UNKNOWN
xc_link     "size_hor"            "ew-resize"        # UNKNOWN
xc_link     "size_ver"            "ns-resize"        # UNKNOWN
xc_cell     "sizing"               8 1               # XFONT
xc_missing  "spider"                                 # XFONT
xc_link     "split_h"             "col-resize"       # UNKNOWN
xc_link     "split_v"             "row-resize"       # UNKNOWN
xc_missing  "spraycan"                               # XFONT
xc_missing  "star"                                   # XFONT
xc_cell     "sw-resize"            7 2               # XDG CSS
xc_cell     "target"               1 6               # XFONT
xc_link     "tcross"              "crosshair"        # XFONT
xc_cell     "text"                 2 0               # XDG CSS
xc_link     "top-left-arrow"      "default"          # UNKNOWN
xc_link     "top-right-arrow"     "right_ptr"        # UNKNOWN
xc_link     "top-tee"             "top_tee"          # UNKNOWN
xc_link     "top_left_arrow"      "right_ptr"        # XFONT
xc_link     "top_left_corner"     "nw-resize"        # XFONT
xc_link     "top_right_corner"    "ne-resize"        # XFONT
xc_link     "top_side"            "n-resize"         # XFONT
xc_cell     "top_tee"              8 3               # XFONT
xc_missing  "trek"                                   # XFONT
xc_cell     "ul_angle"             7 3               # XFONT
xc_missing  "umbrella"                               # XFONT
xc_link     "up"                  "sb_up_arrow"      # UNKNOWN
xc_cell     "up-arrow"             0 1               # XDG
xc_link     "up_arrow"            "up-arrow"         # UNKNOWN
xc_cell     "ur_angle"             9 3               # XFONT
xc_link     "v_double_arrow"      "ns-resize"        # UNKNOWN
xc_cell     "vertical-text"        2 1               # XDG CSS
xc_cell     "w-resize"             7 1               # XDG CSS
xc_cell_ani "wait"                 6 0               # XDG CSS
xc_link     "watch"               "wait"             # XFONT
xc_link     "whats_this"          "help"             # UNKNOWN
xc_link     "x-cursor"            "X_cursor"         # UNKNOWN
xc_cell     "X_cursor"             1 3               # XFONT
xc_link     "xterm"               "text"             # XFONT
xc_cell     "zoom-in"              2 2               # CSS
xc_cell     "zoom-out"             2 3               # CSS

# Hashes
xc_link     "00000000000000020006000e7e9ffc3f" "progress"
xc_link     "00000000017e000002fc000000000000" "text"
xc_link     "00000093000010860000631100006609" "right_ptr"
xc_link     "00008160000006810000408080010102" "ns-resize"
xc_link     "01e00000201c00004038000080300000" "cell"
xc_link     "028006030e0e7ebffc7f7070c0600140" "ew-resize"
xc_link     "03b6e0fcb3499374a867c041f52298f0" "not-allowed"
xc_link     "0426c94ea35c87780ff01dc239897213" "wait"
xc_link     "043a9f68147c53184671403ffa811cc5" "col-resize"
xc_link     "048008013003cff3c00c801001200000" "vertical-text"
xc_link     "0876e1c15ff2fc01f906f1c363074c0f" "link"
xc_link     "08e8e1c95fe2fc01f976f1e063a24ccd" "progress"
xc_link     "08ffe1cb5fe6fc01f906f1c063814ccf" "copy"
xc_link     "08ffe1e65f80fcfdf9fff11263e74c48" "context-menu"
xc_link     "1081e37283d90000800003c07f3ef6bf" "copy"
xc_link     "14fef782d02440884392942c11205230" "col-resize"
xc_link     "208530c400c041818281048008011002" "grabbing"
xc_link     "24020000002800000528000084810000" "default"
xc_link     "2870a09082c103050810ffdffffe0204" "row-resize"
xc_link     "3085a0e285430894940527032f8b26df" "alias"
xc_link     "38c5dff7c7b8962045400281044508d2" "nwse-resize"
xc_link     "3ecb610c1bf2410f44200f48c40d3599" "progress"
xc_link     "4498f0e0c1937ffe01fd06f973665830" "move"
xc_link     "50585d75b494802d0151028115016902" "nesw-resize"
xc_link     "5aca4d189052212118709018842178c0" "grab"
xc_link     "5c6cd98b3f3ebcb1f9c7f1c204630408" "help"
xc_link     "6407b0e94181790501fd1e167b474872" "copy"
xc_link     "640fb0e74195791501fd1ed57b41487f" "alias"
xc_link     "6ce0180090108e0005814700a0021400" "progress"
xc_link     "9081237383d90e509aa00f00170e968f" "move"
xc_link     "9116a3ea924ed2162ecab71ba103b17f" "progress"
xc_link     "9d800788f1b08800ae810202380a0822" "hand1"
xc_link     "a2a266d0498c3104214a47bd64ab0fc8" "link"
xc_link     "b66166c04f8c3109214a4fbd64a50fc8" "copy"
xc_link     "c07385c7190e701020ff7ffffd08103c" "row-resize"
xc_link     "c7088f0f3e6c8088236ef8e1e3e70000" "nwse-resize"
xc_link     "d2201000a2c622004385440041308800" "pointer"
xc_link     "d9ce0ab605698f320427677b458ad60b" "help"
xc_link     "e29285e634086352946a0e7090d73106" "pointer"
xc_link     "f41c0e382c94c0958e07017e42b00462" "zoom-in"
xc_link     "f41c0e382c97c0938e07017e42800402" "zoom-out"
xc_link     "fc618c00da110f0034fd0e004e082400" "wait"
xc_link     "fcf1c3c7cd4491d801f1e1c78f100000" "nesw-resize"
xc_link     "fcf21c00b30f7e3f83fe0dfd12e71cff" "move"


# cleanup
rm -fr "${TMPDIR}"
